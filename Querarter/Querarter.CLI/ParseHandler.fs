﻿namespace Querarter.CLI

open Querarter.CLI.Options
open Querarter.Domain.ChartData
open Querarter.QueryParser.Parser

module ParseHandler =
    let private getQuery (options: Options.Parse) =
        match options.QueryAsGiven with
            | true -> options.Query
            | false -> raise (new System.NotImplementedException())

    let private validate (chart: Chart) =
        raise (new System.NotImplementedException())
        1

    let handleParse (options: Options.Parse) =
        let chart = options |> getQuery |> parse
        printf "Chart definition:\n%A" chart

        match options.Permissive with
        | true -> 0
        | false -> validate chart
