﻿namespace Querarter.CLI

open Querarter.CLI.Options
open Querarter.CLI.ParseHandler
open System

module Program =
    [<EntryPoint>]
    let main argv =
        let mutable handler = fun () -> failwith "Uninitialized behavior's handler."
        let handleVerb (verb: string) (options: obj) =
            match options with
            | null -> ()
            | _ -> handler <-
                    match (Options.ResolveBehavior verb) with
                    | Behavior.Parse -> fun () -> handleParse (options :?> Options.Parse)
                    | other -> failwith (sprintf "Unimplemented behavior: '%s'." (other.ToString()))

        let handlerSetter = new Action<string, Object>(handleVerb)
        let options = new Options()

        let argsParsed =
            CommandLine.Parser.Default
                .ParseArgumentsStrict(argv, options, handlerSetter)

        match argsParsed with
        | false -> 1
        | true -> handler()
