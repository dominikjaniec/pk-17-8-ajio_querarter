﻿namespace Querarter.QueryParserTests

open Querarter.Domain.ChartData
open Querarter.Domain.Transformation
open Querarter.QueryParser.Parser
open Querarter.QueryParser.ParserException
open Xunit

module ParserTests =
    let private assertThatParseToGivenSeriesName inputQuery expectedName =
        match (parse inputQuery) with
        | { Series = { Name = seriesName } } -> Assert.Equal(expectedName, seriesName)

    let private assertThatParseToGivenFromSourceAlias inputQuery expectedAlias =
        match (parse inputQuery) with
        | { Series = { From = { Columns = { Alias = alias } } } } -> Assert.Equal(expectedAlias, alias)

    let assertThatParseToGivenTitles inputQuery top bottom left right =
        match (parse inputQuery) with
        | { Titles = Some { Top = topTitle; Bottom = bottomTitle; LeftSide = leftTitle; RightSide = rightTitle } } ->
            Assert.Equal(top, topTitle)
            Assert.Equal(bottom, bottomTitle)
            Assert.Equal(left, leftTitle)
            Assert.Equal(right, rightTitle)
        | { Titles = None } -> failwith "No parsed Titles."

    let private assertThrowsParserExceptionAt queryInput errorMessage errorLocation =
        try
            let ignored = parse queryInput
            Assert.True(false, "Should throw.")
        with
        | :? ParserException as ex ->
            Assert.Equal(errorMessage, ex.Message)
            Assert.Equal(errorLocation, ex.Location)

    let private assertThrowsParserException queryInput errorMessage =
        assertThrowsParserExceptionAt queryInput errorMessage AtEndOfQuery

    let private assertAreStringifiedEqual expected actual =
        let stringify obj = sprintf "%A" obj
        Assert.Equal(stringify expected, stringify actual)

    [<Fact>]
    let ``Should set Series' name to 'A' when input doesn't have it``() =
        assertThatParseToGivenSeriesName "from 42" "A"

    [<Fact>]
    let ``Should find Series' name from input``() =
        let expectedName = "Series' very own description - \t here could be FROM \n in name of SERIES"
        let inputQuery = sprintf @"SERIES ""%s"" FROM ""some series header""" expectedName
        assertThatParseToGivenSeriesName inputQuery expectedName

    [<Fact>]
    let ``Should set From block's column alias to 'X' when input doesn't have it``() =
        let inputQuery = @"Series ""name"" From ""magic column Y"""
        assertThatParseToGivenFromSourceAlias inputQuery "X"

    [<Fact>]
    let ``Should find From block's column alias from input``() =
        assertThatParseToGivenFromSourceAlias "from 7 as some_alias" "some_alias"

    [<Theory>]
    [<InlineData("from 0", Convert.Identity)>]
    [<InlineData("from 3 converted to identity", Convert.Identity)>]
    [<InlineData(@"from ""case insensitive"" converted to str_LENgth", Convert.StrLength)>]
    [<InlineData(@"from ""case insensitive more"" converted to StR_LeNgTh", Convert.StrLength)>]
    [<InlineData("from 916 converted to STR_length", Convert.StrLength)>]
    [<InlineData("from 9631 converted to timE_poSIX", Convert.TimePosix)>]
    [<InlineData(@"from ""UNIX epoch time"" converted to time_posix", Convert.TimePosix)>]
    [<InlineData(@"series ""uiop"" from ""mars"" aggregated with count", Convert.Identity)>]
    [<InlineData(@"series ""asdf"" from ""Beyond the Mountains"" converted to identity aggregated with count", Convert.Identity)>]
    let ``Should find From block's convert function known qualifier``(inputQuery: string, expectedConvert: Convert) =
        match (parse inputQuery) with
        | { Series = { From = { Convert = convert } } } -> Assert.Equal(expectedConvert, convert)

    [<Fact>]
    let ``Should set Series' Aggregate to None when input dosen't have it``() =
        match (parse @"From ""aggregated with sum""") with
        | { Series = { Aggregate = None } } -> ()
        | { Series = { Aggregate = other } } -> failwith (sprintf "Is aggregated with: %A" other)

    [<Theory>]
    [<InlineData("from 4 aggregated with count", Aggregate.Count)>]
    [<InlineData(@"from ""28"" as column_28 aggregated with count", Aggregate.Count)>]
    [<InlineData(@"from ""numbers"" aggregated with sum", Aggregate.Sum)>]
    [<InlineData(@"from ""insensitive"" aggregated with SuM", Aggregate.Sum)>]
    [<InlineData(@"series ""aggregated by avg"" from ""year: 1989"" aggregated with average", Aggregate.Average)>]
    [<InlineData(@"series ""zxcv"" from 16 converted to str_length aggregated with average", Aggregate.Average)>]
    let ``Should find Series' aggregate function known qualifier``(inputQuery: string, expectedAggregate: Aggregate) =
        match (parse inputQuery) with
        | { Series = { Aggregate = Some aggregate } } -> Assert.Equal(expectedAggregate, aggregate)
        | { Series = { Aggregate = other } } -> failwith (sprintf "Invalid Aggregate qualifier: %A" other)

    [<Fact>]
    let ``Should set Over to None when input dosen't have it``() =
        match (parse @"FROM ""will not be over numbers from 32""") with
        | { Over = None } -> ()
        | { Over = other } -> failwith (sprintf "Has an Over: %A" other)

    [<Theory>]
    [<InlineData("from 64 over numbers from 7 with step 3", 7, 3)>]
    [<InlineData("from 3 over numbers from -1 with step -3", -1, -3)>]
    [<InlineData("from 5 over numbers from -100 with step 100", -100, 100)>]
    [<InlineData(@"from ""Default step is 1"" over numbers from 654321", 654321, 1)>]
    [<InlineData(@"from ""99 Bottles of Beer"" over numbers from 99 with step -1", 99, -1)>]
    let ``Should find Over's correct Numbers' FROM and STEP values``(inputQuery: string, expectedFrom: decimal, expectedStep: decimal) =
        match (parse inputQuery) with
        | { Over = Some (Numbers (from, step)) } ->
            Assert.Equal(expectedFrom, from)
            Assert.Equal(expectedStep, step)
        | { Over = other } -> failwith (sprintf "Invalid Over: %A" other)

    [<Fact>]
    let ``Should set Title to None when input dosen't have it``() =
        match (parse @"FROM ""Not TITLED"" as titled") with
        | { Titles = None } -> ()
        | { Titles = other } -> failwith (sprintf "Has Titles like: %A" other)

    [<Fact>]
    let ``Should find Titles' Top chart title``() =
        let expectedTitle = "Very scientific\t- with multi-line\nlike this one."
        let inputQuery = sprintf @"from 87 titled ""%s""" expectedTitle
        assertThatParseToGivenTitles inputQuery (Some expectedTitle) None None None

    [<Fact>]
    let ``Should find Titles' Bottom chart description``() =
        let bottomTitle = "Some bottom title\tnew line should\nworks also\n..."
        let inputQuery = sprintf @"from 5 described as ""%s""" bottomTitle
        assertThatParseToGivenTitles inputQuery None (Some bottomTitle) None None

    [<Fact>]
    let ``Should find Titles' Left side title``() =
        let leftTitle = "Also with\nlines & t\ta\tb\ts {"
        let inputQuery = sprintf @"from 9 described over left ""%s""" leftTitle
        assertThatParseToGivenTitles inputQuery None None (Some leftTitle) None

    [<Fact>]
    let ``Should find both sides of Titles``() =
        let leftTitle = "left one"
        let rightTitle = "over right"
        let inputQuery = sprintf @"from 11 described over left ""%s"" right ""%s""" leftTitle rightTitle
        assertThatParseToGivenTitles inputQuery None None (Some leftTitle) (Some rightTitle)

    [<Fact>]
    let ``Should find correct sides of Titles for Bottom and Right side description``() =
        let bottomTitle = "titled over numbers from bottom"
        let rightTitle = "over left described as right"
        let inputQuery = sprintf @"from 74 described as ""%s"" over right ""%s""" bottomTitle rightTitle
        assertThatParseToGivenTitles inputQuery None (Some bottomTitle) None (Some rightTitle)

    [<Fact>]
    let ``Should find correct sides of Titles for Top and Left side descriptions``() =
        let topTitle = "master top title"
        let leftTitle = "titled but described as over right"
        let inputQuery = sprintf @"from 38 titled ""%s"" described over left ""%s""" topTitle leftTitle
        assertThatParseToGivenTitles inputQuery (Some topTitle) None (Some leftTitle) None

    [<Fact>]
    let ``Should correctly parse sample query to Chart``() =
        let inputQuery = """
            SERIES "Sample series"
            FROM "header z" AS z_values
            OVER NUMBERS FROM 1
            DESCRIBED AS "Numeric order" OVER RIGHT "Z values"
        """
        let expectedChart = {
            Series = {
                Name = "Sample series"
                From = {
                    Columns = {
                        Source = ColumnHeader "header z"
                        Alias = "z_values"
                    }
                    Convert = Convert.Identity
                }
                Aggregate = None
            }
            Over = Some (Numbers (1M, 1M))
            Titles = Some {
                Top = None
                Bottom = Some "Numeric order"
                LeftSide = None
                RightSide = Some "Z values"
            }
        }
        assertAreStringifiedEqual expectedChart (parse inputQuery)

    [<Fact>]
    let ``Shuold correctly parse sample query with aggregate to Chart``() =
        let inputQuery = """
            from 14 aggregated with sum
            titled "Sum of 15th column"
        """
        let expectedChart = {
            Series = {
                Name = "A"
                From = {
                    Columns = {
                        Source = ColumnIndex 14
                        Alias = "X"
                    }
                    Convert = Convert.Identity
                }
                Aggregate = Some Aggregate.Sum
            }
            Over = None
            Titles = Some {
                Top = Some "Sum of 15th column"
                Bottom = None
                LeftSide = None
                RightSide = None
            }
        }
        assertAreStringifiedEqual expectedChart (parse inputQuery)

    [<Fact>]
    let ``Should correctly parse sample query with convertion to Chart``() =
        let inputQuery = """
            Series "magic" From "second" Converted to str_length
            Described over left "Row's length"
            """
        let expectedChart = {
            Series = {
                Name = "magic"
                From = {
                    Columns = {
                        Source = ColumnHeader "second"
                        Alias = "X"
                    }
                    Convert = Convert.StrLength
                }
                Aggregate = None
            }
            Over = None
            Titles = Some {
                Top = None
                Bottom = None
                LeftSide = Some "Row's length"
                RightSide = None
            }
        }
        assertAreStringifiedEqual expectedChart (parse inputQuery)

    [<Fact>]
    let ``Should correctly parse sample full not aggregated query to Chart``() =
        let inputQuery = """
            series "sample single series"
            from "1234" as its_name converted to time_posix
            over numbers from 13 with step 7
            titled "Not aggregated query's chart"
            described as "Some numbers like 13, 20, 27, 34..."
            over left "Check other side's description"
            right "Column '1234' as Date/Time"
        """
        let expectedChart = {
            Series = {
                Name = "sample single series"
                From = {
                    Columns = {
                        Source = ColumnHeader "1234"
                        Alias = "its_name"
                    }
                    Convert = Convert.TimePosix
                }
                Aggregate = None
            }
            Over = Some (Numbers (13M, 7M))
            Titles = Some {
                Top = Some "Not aggregated query's chart"
                Bottom = Some "Some numbers like 13, 20, 27, 34..."
                LeftSide = Some "Check other side's description"
                RightSide = Some "Column '1234' as Date/Time"
            }
        }
        assertAreStringifiedEqual expectedChart (parse inputQuery)

    [<Fact>]
    let ``Should parse query as its all is required``() =
        let inputQuery = """
            SERIES "Series' name for full but not 'correct' chart"
            FROM 5 AS source_alias CONVERTED TO str_length
            AGGREGATED WITH average OVER NUMBERS FROM 87 WITH STEP 2
            TITLED "Title of chart" DESCRIBED AS "X-Axis' description"
            OVER LEFT "Left side of Y-Axis' title" RIGHT "Right side of Y-Axis"
        """
        let expectedChart = {
            Series = {
                Name = "Series' name for full but not 'correct' chart"
                From = {
                    Columns = {
                        Source = ColumnIndex 5
                        Alias = "source_alias"
                    }
                    Convert = Convert.StrLength
                }
                Aggregate = Some Aggregate.Average
            }
            Over = Some (Numbers (87M, 2M))
            Titles = Some {
                Top = Some "Title of chart"
                Bottom = Some "X-Axis' description"
                LeftSide = Some "Left side of Y-Axis' title"
                RightSide = Some "Right side of Y-Axis"
            }
        }
        assertAreStringifiedEqual expectedChart (parse inputQuery)

    [<Theory>]
    [<InlineData("")>]
    [<InlineData("     ")>]
    [<InlineData("\t")>]
    [<InlineData("    \t")>]
    let ``Should raise for an empty input``(input: string) =
        assertThrowsParserException input errorEmptyQuery


    // Should be the Parser responsibility:

    //[<Theory>]
    //[<InlineData(@"""invalid""block", 8)>]
    //[<InlineData(@"in""between""nodes", 2)>]
    //[<InlineData(@"other""bad one sample""", 5)>]
    //[<InlineData(@"looks mostly OK""but isn't""", 15)>]
    //[<InlineData(@"empty \talso\t \t\tbad""""", 18)>]
    //let ``Should throws TextException for stuck Text blocks``(input, errorPosition) =
    //    assertTextException Tokenizer.errorStuckTextBlock errorPosition input

    //[<Theory>]
    //[<InlineData(@"""""", 0)>]
    //[<InlineData(@""""" more", 0)>]
    //[<InlineData(@"x_staff """"", 8)>]
    //[<InlineData(@" \t some other """" bum", 14)>]
    //[<InlineData(@"magic ""with near"" """" block", 18)>]
    //let ``Should throws TextException for empty Text blocks``(input, errorPosition) =

    //    assertTextException Tokenizer.errorEmptyTextBlock errorPosition input
