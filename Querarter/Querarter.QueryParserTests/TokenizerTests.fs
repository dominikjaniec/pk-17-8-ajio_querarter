﻿namespace Querarter.QueryParserTests

open Querarter.QueryParser.Tokenizer
open Xunit

module TokenizerTests =
    let private assertStringifiedSeqencesAreEqual expected actual =
        let stringify seq = Seq.map (fun x -> sprintf "%A" x) seq |> (String.concat " ")
        Assert.Equal(expected |> stringify, actual |> stringify)

    [<Fact>]
    let ``Should return Token's list for the input``() =
        match (chopTokens ".") with
        | [ Word x ] -> Assert.Equal(".", x)
        | a -> failwith (sprintf "Invalid returned value: %A" a)

    [<Theory>]
    [<InlineData(" ")>]
    [<InlineData("\n")>]
    [<InlineData(" \t    ")>]
    [<InlineData("\t")>]
    [<InlineData("\t \n")>]
    [<InlineData("      \t")>]
    [<InlineData("\t \t \t \t \t")>]
    let ``Should return Blank Token from sequence of white spaces``(input: string) =
        match (chopTokens input) with
        | [ Blank size ] -> Assert.Equal((String.length input), size)
        | a -> failwith (sprintf "Invalid returned value: %A" a)

    [<Theory>]
    [<InlineData("x")>]
    [<InlineData("1q2w3e")>]
    [<InlineData("UIOP")>]
    [<InlineData("42xyZ")>]
    [<InlineData("abcdefg")>]
    [<InlineData("_qwer2")>]
    [<InlineData("mn_")>]
    let ``Should return Word Token from sequence of word characters``(input: string) =
        match (chopTokens input) with
        | [ Word token ] -> Assert.Equal(token, input)
        | a -> failwith (sprintf "Invalid returned value: %A" a)

    [<Fact>]
    let ``Should chop input between text Words' and Blanks' spaces with its index position``() =
        let input = "word\t\tnot_4_space 1234   zxcv42 FromBigCHAR \t321asdf    "
        let expected = [
            { Token = Word "word"; Index = 0 };
            { Token = Blank 2; Index = 4 };
            { Token = Word "not_4_space"; Index = 6 };
            { Token = Blank 1; Index = 17 };
            { Token = Word "1234"; Index = 18 };
            { Token = Blank 3; Index = 22 };
            { Token = Word "zxcv42"; Index = 25 };
            { Token = Blank 1; Index = 31 };
            { Token = Word "FromBigCHAR"; Index = 32 };
            { Token = Blank 2; Index = 43 };
            { Token = Word "321asdf"; Index = 45 };
            { Token = Blank 4; Index = 52 }
        ]
        assertStringifiedSeqencesAreEqual expected (chop input)

    [<Fact>]
    let ``Should return Text Token from quoted block of text``() =
        match (chopTokens @"""test sentence""") with
        | [ Text textBlock ] -> Assert.Equal(textBlock, "test sentence")
        | a -> failwith (sprintf "Invalid returned value: %A" a)

    [<Fact>]
    let ``Shuold handle multiple Text blocks with its index position``() =
        let input = @"sample ""block of text"" with ""multiple"" ""text blocks'"""
        let expected = [
            { Token = Word "sample"; Index = 0 };
            { Token = Blank 1; Index = 6 };
            { Token = Text "block of text"; Index = 7 };
            { Token = Blank 1; Index = 22 };
            { Token = Word "with"; Index = 23 };
            { Token = Blank 1; Index = 27 };
            { Token = Text "multiple"; Index = 28 };
            { Token = Blank 1; Index = 38 };
            { Token = Text "text blocks'"; Index = 39 }
        ]
        assertStringifiedSeqencesAreEqual expected (chop input)

    [<Theory>]
    [<InlineData(@"""")>]
    [<InlineData(@"some ""missing one")>]
    [<InlineData(@"""never ending story")>]
    [<InlineData(@"more chaotic"" staff")>]
    [<InlineData(@"first""stuck item")>]
    [<InlineData(@"stuck and""bad")>]
    [<InlineData(@"""looks like ""inside""")>]
    [<InlineData(@"other ""with""inner"" problems")>]
    let ``Should throw TextException for unclosed quotation mark``(input) =
        try
            let ignored = chopTokens input
            Assert.True(false, "Should throw.")
        with
        | :? System.InvalidOperationException as ex -> Assert.Equal(errorMissingClosingQuote, ex.Message)
