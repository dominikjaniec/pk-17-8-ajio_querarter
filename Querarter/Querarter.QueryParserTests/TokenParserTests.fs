﻿namespace Querarter.QueryParserTests

open Querarter.Domain.Syntax
open Querarter.QueryParser.Tokenizer
open Querarter.QueryParser.TokenParser
open Xunit

module TokenParserTests =
    [<Theory>]
    [<InlineData("{", Keyword.CodeBlockBegin)>]
    [<InlineData("}", Keyword.CodeBlockEnd)>]
    [<InlineData("SERIES", Keyword.SERIES)>]
    [<InlineData("FROM", Keyword.FROM)>]
    [<InlineData("AS", Keyword.AS)>]
    [<InlineData("CONVERTED", Keyword.CONVERTED)>]
    [<InlineData("TO", Keyword.TO)>]
    [<InlineData("AGGREGATED", Keyword.AGGREGATED)>]
    [<InlineData("WITH", Keyword.WITH)>]
    [<InlineData("OVER", Keyword.OVER)>]
    [<InlineData("NUMBERS", Keyword.NUMBERS)>]
    [<InlineData("STEP", Keyword.STEP)>]
    [<InlineData("VALUES", Keyword.VALUES)>]
    [<InlineData("TITLED", Keyword.TITLED)>]
    [<InlineData("DESCRIBED", Keyword.DESCRIBED)>]
    [<InlineData("LEFT", Keyword.LEFT)>]
    [<InlineData("RIGHT", Keyword.RIGHT)>]
    [<InlineData("ConVerTed", Keyword.CONVERTED)>]
    [<InlineData("aggreGATED", Keyword.AGGREGATED)>]
    [<InlineData("oVeR", Keyword.OVER)>]
    [<InlineData("Series", Keyword.SERIES)>]
    [<InlineData("NUMbers", Keyword.NUMBERS)>]
    [<InlineData("tO", Keyword.TO)>]
    [<InlineData("As", Keyword.AS)>]
    [<InlineData("with", Keyword.WITH)>]
    [<InlineData("described", Keyword.DESCRIBED)>]
    [<InlineData("step", Keyword.STEP)>]
    [<InlineData("steP", Keyword.STEP)>]
    [<InlineData("FROM", Keyword.FROM)>]
    [<InlineData("from", Keyword.FROM)>]
    [<InlineData("From", Keyword.FROM)>]
    [<InlineData("fROM", Keyword.FROM)>]
    [<InlineData("FRom", Keyword.FROM)>]
    [<InlineData("FroM", Keyword.FROM)>]
    [<InlineData("fRoM", Keyword.FROM)>]
    [<InlineData("fram", Keyword.UnknownKeyword)>]
    [<InlineData("magic", Keyword.UnknownKeyword)>]
    [<InlineData("f.r.o.m", Keyword.UnknownKeyword)>]
    [<InlineData("f  r o  m", Keyword.UnknownKeyword)>]
    [<InlineData("F R   O M", Keyword.UnknownKeyword)>]
    [<InlineData("more magic staff", Keyword.UnknownKeyword)>]
    [<InlineData("agregated", Keyword.UnknownKeyword)>]
    [<InlineData("setp", Keyword.UnknownKeyword)>]
    [<InlineData("ot", Keyword.UnknownKeyword)>]
    [<InlineData("vvith", Keyword.UnknownKeyword)>]
    [<InlineData("some other staff", Keyword.UnknownKeyword)>]
    [<InlineData("series from described", Keyword.UnknownKeyword)>]
    [<InlineData("", Keyword.UnknownKeyword)>]
    [<InlineData(".", Keyword.UnknownKeyword)>]
    [<InlineData("a_s", Keyword.UnknownKeyword)>]
    [<InlineData("con.ver.ted", Keyword.UnknownKeyword)>]
    [<InlineData("####", Keyword.UnknownKeyword)>]
    [<InlineData("!@#$", Keyword.UnknownKeyword)>]
    [<InlineData(")(*&", Keyword.UnknownKeyword)>]
    [<InlineData("   ", Keyword.UnknownKeyword)>]
    [<InlineData("", Keyword.UnknownKeyword)>]
    [<InlineData("\t", Keyword.UnknownKeyword)>]
    [<InlineData("\n", Keyword.UnknownKeyword)>]
    [<InlineData("{{", Keyword.UnknownKeyword)>]
    [<InlineData("}}", Keyword.UnknownKeyword)>]
    [<InlineData("UnknownKeyword", Keyword.UnknownKeyword)>]
    [<InlineData("CodeBlockBegin", Keyword.UnknownKeyword)>]
    [<InlineData("CodeBlockEnd", Keyword.UnknownKeyword)>]
    let ``Should resolve Keyword with case insensitive from a string``(word: string, expectedKeyword: Keyword) =
        Assert.Equal(expectedKeyword, (keywordFrom word))

    [<Fact>]
    let ``Should parse Blank Token as SpaceToken``() =
        let blankTokenData = { Token = Blank 42; Index = 7 }
        match (parseToken blankTokenData) with
        | SpaceToken _ -> ()
        | a -> failwith (sprintf "Invalid returned value: %A" a)

    [<Fact>]
    let ``Should parse Word Token as KeywordToken when it's a correct keyword``() =
        let wordTokenData = { Token = Word "converted"; Index = 13 }
        match (parseToken wordTokenData) with
        | KeywordToken data ->
            Assert.Equal(data.Keyword, Keyword.CONVERTED)
            Assert.Equal(data.Index, wordTokenData.Index)
        | a -> failwith (sprintf "Invalid returned value: %A" a)

    [<Fact>]
    let ``Should parse Word Token as QualifierToken when it isn't a correct keyword``() =
        let wordTokenData = { Token = Word "user"; Index = 7 }
        match (parseToken wordTokenData) with
        | QualifierToken data ->
            Assert.Equal(data.Value, "user")
            Assert.Equal(data.Index, wordTokenData.Index)
        | a -> failwith (sprintf "Invalid returned value: %A" a)

    [<Fact>]
    let ``Should parse Text Token as TextToken``() =
        let textTokenData = { Token = Text "some magic staff"; Index = 3 }
        match (parseToken textTokenData) with
        | TextToken data ->
            Assert.Equal(data.Value, "some magic staff")
            Assert.Equal(data.Index, textTokenData.Index)
        | a -> failwith (sprintf "Invalid returned value: %A" a)
