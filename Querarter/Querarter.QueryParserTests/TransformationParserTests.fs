﻿namespace Querarter.QueryParserTests

open Querarter.Domain.Transformation
open Querarter.QueryParser.TransformationParser
open Xunit

module TransformationParserTests =
    [<Theory>]
    [<InlineData("average", Aggregate.Average)>]
    [<InlineData("count", Aggregate.Count)>]
    [<InlineData("sum", Aggregate.Sum)>]
    [<InlineData("Average", Aggregate.Average)>]
    [<InlineData("CoUnT", Aggregate.Count)>]
    [<InlineData("SUM", Aggregate.Sum)>]
    [<InlineData("averAGE", Aggregate.Average)>]
    [<InlineData("COUNT", Aggregate.Count)>]
    [<InlineData("sUm", Aggregate.Sum)>]
    [<InlineData("sun", Aggregate.UnknownAggregate)>]
    [<InlineData("c0unt", Aggregate.UnknownAggregate)>]
    [<InlineData("avg", Aggregate.UnknownAggregate)>]
    [<InlineData("age", Aggregate.UnknownAggregate)>]
    [<InlineData("magic", Aggregate.UnknownAggregate)>]
    [<InlineData("some-staff", Aggregate.UnknownAggregate)>]
    let ``Should resolve Aggregate with case insensitive from a string``(word: string, expectedAggregate: Aggregate) =
        Assert.Equal(expectedAggregate, (aggregateFrom word))

    [<Theory>]
    [<InlineData("identity", Convert.Identity)>]
    [<InlineData("str_length", Convert.StrLength)>]
    [<InlineData("time_posix", Convert.TimePosix)>]
    [<InlineData("Identity", Convert.Identity)>]
    [<InlineData("STR_Length", Convert.StrLength)>]
    [<InlineData("time_POSIX", Convert.TimePosix)>]
    [<InlineData("idENTITy", Convert.Identity)>]
    [<InlineData("str_LENGTH", Convert.StrLength)>]
    [<InlineData("tIme_PoSiX", Convert.TimePosix)>]
    [<InlineData("time-posix", Convert.UnknownConvert)>]
    [<InlineData("idontity", Convert.UnknownConvert)>]
    [<InlineData("length", Convert.UnknownConvert)>]
    [<InlineData("t1me_p051x", Convert.UnknownConvert)>]
    [<InlineData("magic", Convert.UnknownConvert)>]
    let ``Should resolve Convert with case insensitive from a string``(word: string, expectedConvert: Convert) =
        Assert.Equal(expectedConvert, (convertFrom word))
