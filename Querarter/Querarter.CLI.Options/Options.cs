﻿using System;
using CommandLine;
using CommandLine.Text;

namespace Querarter.CLI.Options
{
    public class Options
    {
        private const string MakeBehavior = "make";
        private const string MakeDescription = "Make chart based on query and data source.";

        private const string ParseBehavior = "parse";
        private const string ParseDescription = "Parse given query and show chart's description.";

        private const string ConvertBehavior = "convert";
        private const string ConvertDescription = "Convert data source with the aid of query.";

        [VerbOption(MakeBehavior, HelpText = MakeDescription)]
        public Make MakeVerb { get; set; }

        [VerbOption(ParseBehavior, HelpText = ParseDescription)]
        public Parse ParseVerb { get; set; }

        [VerbOption(ConvertBehavior, HelpText = ConvertDescription)]
        public Convert ConvertVerb { get; set; }

        [HelpVerbOption]
        public string Usages(string verb)
        {
            switch (BehaviorFrom(verb))
            {
                case Behavior.Make:
                    return HelpTextForVerb(MakeBehavior, MakeDescription, MakeVerb);

                case Behavior.Parse:
                    return HelpTextForVerb(ParseBehavior, ParseDescription, ParseVerb);

                case Behavior.Convert:
                    return HelpTextForVerb(ConvertBehavior, ConvertDescription, ConvertVerb);

                default:
                    return HelpTextForApp(this);
            }
        }

        public static Behavior ResolveBehavior(string verb)
        {
            var behavior = BehaviorFrom(verb);
            if (behavior != Behavior.Unknown)
                return behavior;

            throw new ArgumentException($"Unknown verb behavior: '{verb ?? "{null}"}'.", nameof(verb));
        }

        private static Behavior BehaviorFrom(string verb)
        {
            switch (verb?.ToLowerInvariant())
            {
                case MakeBehavior:
                    return Behavior.Make;

                case ParseBehavior:
                    return Behavior.Parse;

                case ConvertBehavior:
                    return Behavior.Convert;

                default:
                    return Behavior.Unknown;
            }
        }

        private static HelpText CreateHelpTextAs(Action<HelpText> configuration)
        {
            var helpText = new HelpText
            {
                Heading = new HeadingInfo(App.FullName, App.Version),
                Copyright = new CopyrightInfo(App.Author, App.Years),
                AddDashesToOption = true,
                AdditionalNewLineAfterOption = false
            };

            configuration(helpText);
            return helpText;
        }

        private static HelpText HelpTextForApp(Options instance)
        {
            return CreateHelpTextAs(helpText =>
            {
                helpText.AddDashesToOption = false;
                helpText.AddOptions(instance);
            });
        }

        private static HelpText HelpTextForVerb(string verb, string description, BaseOptions options)
        {
            return CreateHelpTextAs(helpText =>
            {
                var nl = Environment.NewLine;
                var execution = $"`$ {App.ExecName} {verb}`";
                var headline = $"{nl}{execution} - {description}";
                helpText.AddPreOptionsLine(headline);

                helpText.AddOptions(options);
            });
        }

        public abstract class BaseOptions
        {
        }

        public abstract class BaseQuery : BaseOptions
        {
            [Option('q', "query", HelpText = "Given query's file path.", Required = true)]
            public string Query { get; set; }

            [Option('g', HelpText = "Query will be interpreted as given, not as file path.", DefaultValue = false)]
            public bool QueryAsGiven { get; set; }
        }

        public abstract class BaseTransformation : BaseQuery
        {
            [Option('i', "input", HelpText = "Input source data's file path, otherwise STDIN will be used instead as source.")]
            public string Input { get; set; }

            [Option('s', "input-format", HelpText = "Source data's file format.", DefaultValue = "csv")]
            public string InputFormat { get; set; }

            [Option('o', "output", HelpText = "Generated result's file path.", Required = true)]
            public string Output { get; set; }

            [Option('f', "override", HelpText = "Force override generated output.", DefaultValue = false)]
            public bool OverrideOutput { get; set; }
        }

        public class Make : BaseTransformation
        {
            [Option('e', "output-format", HelpText = "Generated chart's file format.", DefaultValue = "png")]
            public string OutputFormat { get; set; }
        }

        public class Parse : BaseQuery
        {
            [Option('p', "permissive", HelpText = "Skip chart's logic validation.", DefaultValue = false)]
            public bool Permissive { get; set; }
        }

        public class Convert : BaseTransformation
        {
            [Option('e', "output-format", HelpText = "Converted data's file format.", DefaultValue = "csv")]
            public string OutputFormat { get; set; }
        }
    }
}
