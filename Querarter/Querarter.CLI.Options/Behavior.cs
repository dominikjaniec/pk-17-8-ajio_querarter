﻿namespace Querarter.CLI.Options
{
    public enum Behavior
    {
        Unknown,
        Make,
        Parse,
        Convert
    }
}
