﻿namespace Querarter.CLI.Options
{
    public static class App
    {
        public const string Name = "Querarter";
        public const string Description = "Query Based Chart Generator";
        public const string FullName = Name + ": " + Description;

        public const string Author = "Dominik Janiec";
        public static readonly int[] Years = { 2017 };

        public static string Version => "v. alpha-0.0.0.1";
        public static string ExecName => Name.ToLowerInvariant();
    }
}
