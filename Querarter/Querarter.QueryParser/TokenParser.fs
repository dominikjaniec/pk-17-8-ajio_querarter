﻿namespace Querarter.QueryParser

open Querarter.Domain.Syntax
open Querarter.QueryParser.Tokenizer

module TokenParser =
    type KeywordData = { Index: int; Keyword: Keyword }
    type UserData = { Index: int; Value: string }

    type Token =
        | KeywordToken of KeywordData
        | QualifierToken of UserData
        | TextToken of UserData
        | SpaceToken of int
        | EndOfQueryToken

    let asDecimal (data: UserData) =
        data.Value |> System.Decimal.Parse

    let asInt (data: UserData) =
        data.Value |> System.Int32.Parse

    let keywordFrom (word: string) =
        match (word.ToUpperInvariant()) with
        | "{" -> Keyword.CodeBlockBegin
        | "}" -> Keyword.CodeBlockEnd
        | "SERIES" -> Keyword.SERIES
        | "FROM" -> Keyword.FROM
        | "AS" -> Keyword.AS
        | "CONVERTED" -> Keyword.CONVERTED
        | "TO" -> Keyword.TO
        | "AGGREGATED" -> Keyword.AGGREGATED
        | "WITH" -> Keyword.WITH
        | "OVER" -> Keyword.OVER
        | "NUMBERS" -> Keyword.NUMBERS
        | "STEP" -> Keyword.STEP
        | "VALUES" -> Keyword.VALUES
        | "TITLED" -> Keyword.TITLED
        | "DESCRIBED" -> Keyword.DESCRIBED
        | "LEFT" -> Keyword.LEFT
        | "RIGHT" -> Keyword.RIGHT
        | _ -> Keyword.UnknownKeyword

    let toString (keyword: Keyword) =
        match keyword with
        | Keyword.CodeBlockBegin -> "{"
        | Keyword.CodeBlockEnd -> "}"
        | Keyword.SERIES -> "SERIES"
        | Keyword.FROM -> "FROM"
        | Keyword.AS -> "AS"
        | Keyword.CONVERTED -> "CONVERTED"
        | Keyword.TO -> "TO"
        | Keyword.AGGREGATED -> "AGGREGATED"
        | Keyword.WITH -> "WITH"
        | Keyword.OVER -> "OVER"
        | Keyword.NUMBERS -> "NUMBERS"
        | Keyword.STEP -> "STEP"
        | Keyword.VALUES -> "VALUES"
        | Keyword.TITLED -> "TITLED"
        | Keyword.DESCRIBED -> "DESCRIBED"
        | Keyword.LEFT -> "LEFT"
        | Keyword.RIGHT -> "RIGHT"
        | other -> failwith (sprintf "Keyword: %A does not have a text representation." other)

    let parseToken (data: TokenData) =
        match data with
        | { Token = Blank index } -> SpaceToken index
        | { Token = Text text } -> TextToken { Index = data.Index; Value = text }
        | { Token = Word word } ->
            match (keywordFrom word) with
            | Keyword.UnknownKeyword -> QualifierToken { Index = data.Index; Value = word }
            | keyword -> KeywordToken { Index = data.Index; Keyword = keyword }
