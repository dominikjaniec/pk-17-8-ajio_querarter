﻿namespace Querarter.QueryParser

open Microsoft.FSharp.Reflection
open Querarter.Domain.Syntax
open Querarter.QueryParser.TokenParser

module ParserException =

    let errorEmptyQuery = "Query could not be empty."
    let errorNotParsedQuery = "Query could not be parsed."

    type ErrorLocation =
        | AtIndex of int
        | AtEndOfQuery

    type ParserException (location: ErrorLocation, message: string) =
        inherit System.InvalidOperationException(message)
        member this.Location = location

    let raiseEmptyQueryException() =
        raise (ParserException (AtEndOfQuery, errorEmptyQuery))

    let raiseCouldNotParseQueryException() =
        raise (ParserException (AtEndOfQuery, errorNotParsedQuery))

    let private extractSample str =
        let maxStringSize = 16
        if (String.length str <= maxStringSize) then
            str
        else
            let size = maxStringSize - 3
            let sample = str.Substring(0, size)
            sprintf "%s..." sample

    let raiseUnexpectedTokenException (group: GroupId) (expectedNode: SyntaxNode) (actual: Token) =
        let keywordStr = "keyword"
        let choiceStr = "choice"
        let qualifierStr = "user qualifier"
        let userTextStr = "block of text"
        let spaceStr = "block of space"
        let eoqStr = "end of query"

        let whatKeyword keyword =
            toString keyword |> sprintf "%s: '%s'" keywordStr

        let whatQualifier qualifier =
            extractSample qualifier |> sprintf "%s like '%s'" qualifierStr

        let whatUserText userText =
            extractSample userText |> sprintf @"%s like ""%s""" userTextStr

        let whatEncounterAtIndex what index =
            let encounter what index =
                sprintf "%s at index: %d" what index
            ((encounter what index), AtIndex index)

        let (encounter, whereLocation) =
            match actual with
            | KeywordToken data -> whatEncounterAtIndex (whatKeyword data.Keyword) data.Index
            | QualifierToken data -> whatEncounterAtIndex (whatQualifier data.Value) data.Index
            | TextToken data -> whatEncounterAtIndex (whatUserText data.Value) data.Index
            | SpaceToken index -> whatEncounterAtIndex spaceStr index
            | EndOfQueryToken -> (eoqStr, AtEndOfQuery)

        let expected =
            match expectedNode with
            | Keyword keyword -> whatKeyword keyword
            | Qualifier -> qualifierStr
            | UserText -> userTextStr
            | Choice _ -> choiceStr
            | node -> sprintf "%A" node

        let groupName =
            match FSharpValue.GetUnionFields(group, typeof<GroupId>) with
            | (case, _) -> case.Name

        let message = sprintf "Encounter %s, where expecting %s in the group: '%s'." encounter expected groupName
        raise (ParserException (whereLocation, message))
