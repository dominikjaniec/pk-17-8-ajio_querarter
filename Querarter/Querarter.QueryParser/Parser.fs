﻿namespace Querarter.QueryParser

open Querarter.Domain.ChartData
open Querarter.Domain.ChartSetter
open Querarter.Domain.Syntax
open Querarter.Domain.Transformation
open Querarter.QueryParser.Tokenizer
open Querarter.QueryParser.TokenParser
open Querarter.QueryParser.TransformationParser
open Querarter.QueryParser.ParserException

module Parser =
    let private basicChart =
        let column = { Source = ColumnIndex 0; Alias = "X" }
        let from = { Columns = column; Convert = Convert.Identity }
        let series = { Name = "A"; From = from; Aggregate = None }
        { Series = series; Over = None; Titles = None }

    let private setChartBasedOn (group: GroupId) (token: Token) (chart: Chart) =
       match (group, token) with
        | (SeriesName, TextToken data) -> withSeriesNameAs chart data.Value
        | (SeriesFrom, TextToken data) -> withSeriesFromColumnsSourceAs chart (ColumnHeader data.Value)
        | (SeriesFrom, QualifierToken data) -> withSeriesFromColumnsSourceAs chart (ColumnIndex (asInt data))
        | (SeriesFromAs, QualifierToken data) -> withSeriesFromColumnAliasAs chart data.Value
        | (SeriesFromConvertedTo, QualifierToken data) -> withSeriesFromConvertAs chart (convertFrom data.Value)
        | (SeriesAggregateWith, QualifierToken data) -> withSeriesAggregateAs chart (aggregateFrom data.Value)
        | (OverNumbersFrom, QualifierToken data) -> withOverNumbersFromAs chart (asDecimal data)
        | (OverNumbersFromWithStep, QualifierToken data) -> withOverNumbersFromWithStepAs chart (asDecimal data)
        | (Titled, TextToken data) -> withTitlesTopAs chart data.Value
        | (DescribedAs, TextToken data) -> withTitlesBottomAs chart data.Value
        | (DescribedOverLeft, TextToken data) -> withTitlesLeftSideAs chart data.Value
        | (DescribedOverRight, TextToken data) -> withTitlesRightSideAs chart data.Value
        | _ -> failwith (sprintf "Could not set a Chart from data: (GroupId: %A, Token: %A)." group token)

    type private TokenMatch =
        | TokenMismatch
        | KeywordMismatch
        | GroupMismatch
        | Matched

    let private createChartQuery (tokens: Token list)  =
        let mutable currentToken = EndOfQueryToken
        let mutable leftTokens = tokens
        let mutable contextGroup = UnknownGroup
        let mutable contextGroupReq = false
        let mutable contextStack: GroupDefinition<SyntaxNode> list = []
        let mutable parsedChart = basicChart

        let popCurrentTokenAs tokenMatch =
            currentToken <- leftTokens |> List.head
            leftTokens <- leftTokens |> List.tail
            tokenMatch

        let pushContext groupDef =
            contextStack <- groupDef :: contextStack
            contextGroup <- groupDef.Id
            contextGroupReq <- groupDef.Req

        let popContext() =
            contextStack <- contextStack |> List.tail
            match (contextStack |> List.tryHead) with
            | Some context ->
                contextGroup <- context.Id
                contextGroupReq <- context.Req
            | None ->
                contextGroup <- UnknownGroup
                contextGroupReq <- false

        let rec matchSyntax (syntaxNode: SyntaxNode) =
            let raiseInvalidCurrentTokenException() =
                raiseUnexpectedTokenException contextGroup syntaxNode currentToken

            let matchGroup groupDef =
                let notMatchingSingleElement node =
                    match (matchSyntax node) with
                    | KeywordMismatch | TokenMismatch -> Some ()
                    | _ -> None

                let tryMatchGroup nodes =
                    match (nodes |> List.tryPick notMatchingSingleElement) with
                    | None -> Matched
                    | Some _ ->
                        match contextGroupReq with
                        | false -> GroupMismatch
                        | true -> raiseInvalidCurrentTokenException()

                pushContext groupDef
                let groupMatch = tryMatchGroup groupDef.Nodes
                popContext()
                groupMatch

            let matchChoice nodes =
                let tryMatchChoice node =
                    match (matchSyntax node) with
                    | Matched -> true
                    | _ -> false

                match (nodes |> List.tryFind tryMatchChoice) with
                | Some _ -> Matched
                | None -> raiseInvalidCurrentTokenException()

            let matchKeyword syntaxKey tokenKey =
                match (syntaxKey = tokenKey) with
                | true -> popCurrentTokenAs Matched
                | false -> KeywordMismatch

            let matchUserToken() =
                parsedChart <- setChartBasedOn contextGroup currentToken parsedChart
                popCurrentTokenAs Matched

            match (syntaxNode, currentToken) with
            | (Group def, _) -> matchGroup def
            | (Choice nodes, _) -> matchChoice nodes
            | (Keyword syntax, KeywordToken { Keyword = current }) -> matchKeyword syntax current
            | (Qualifier, QualifierToken _) | (UserText, TextToken _) -> matchUserToken()
            | _ -> TokenMismatch

        popCurrentTokenAs TokenMismatch |> ignore

        match (matchSyntax syntaxTree) with
        | Matched -> parsedChart
        | _ -> raiseCouldNotParseQueryException()

    let private getTokens query =
        let notSpace parsedToken =
            match parsedToken with
            | SpaceToken _ -> false
            | _ -> true

        let givenTokens =
            query
                |> chop
                |> List.map parseToken
                |> List.filter notSpace

        match (List.isEmpty givenTokens) with
        | false -> givenTokens @ [ EndOfQueryToken ]
        | true -> raiseEmptyQueryException()

    let parse query =
        query
            |> getTokens
            |> createChartQuery
