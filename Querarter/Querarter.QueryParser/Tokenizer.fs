﻿namespace Querarter.QueryParser

module Tokenizer =
    let errorMissingClosingQuote = "Missing closing quotation mark."

    type Token =
        | Blank of int
        | Text of string
        | Word of string

    type TokenData = {
        Token: Token
        Index: int
    }

    type private CharKind =
        | UndefinedChar
        | SpaceChar
        | QuoteChar
        | WordChar

    let private charKind char =
        if (System.Char.IsWhiteSpace char) then
            SpaceChar
        elif char = '"' then
            QuoteChar
        else
            WordChar

    type private State =
        | UnknownState
        | SpaceState
        | TextState
        | WordState

    let private toState kind =
        match kind with
        | SpaceChar -> SpaceState
        | QuoteChar -> TextState
        | WordChar -> WordState
        | _ -> UnknownState

    let private createTokenFrom str state =
        match state with
        | SpaceState -> String.length str |> Blank
        | TextState -> Text str
        | WordState -> Word str
        | _ -> failwith "Could not create Token from an invalid state."

    let chop (input: string) =
        let inputLength = String.length input
        let mutable tokens: TokenData list = []
        let mutable buffer: char list = []

        let pushToBuffer char =
           buffer <- char :: buffer

        let flushBufferToTokens revIdx state =
            let str = new string (buffer |> Array.ofList)
            let token = createTokenFrom str state
            let index = inputLength - revIdx
            tokens <- { Token = token; Index = index } :: tokens
            buffer <- []

        let mutable previousKind = UndefinedChar
        let mutable state = UnknownState

        let switchStateTo char revIdx =
            if state <> UnknownState then
                flushBufferToTokens revIdx state
            state <- char |> charKind |> toState

        let switchStateFromText revIdx =
            flushBufferToTokens (revIdx + 1) TextState
            state <- UnknownState

        let iterator revIdx char =
            let kind = charKind char
            if state <> TextState then
                if kind = QuoteChar then
                    switchStateTo char revIdx
                else
                    if kind <> previousKind then
                        switchStateTo char revIdx
                    pushToBuffer char
            else
                if kind = QuoteChar then
                    switchStateFromText revIdx
                else
                    pushToBuffer char
            previousKind <- kind

        Seq.rev input
            |> Seq.iteri iterator

        match state with
        | UnknownState -> ()
        | SpaceState | WordState -> flushBufferToTokens inputLength state
        | TextState ->  invalidOp errorMissingClosingQuote

        tokens

    let chopTokens input =
        chop input
            |> List.map (fun data -> data.Token)
