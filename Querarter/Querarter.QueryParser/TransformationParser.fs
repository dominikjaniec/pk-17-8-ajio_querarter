﻿namespace Querarter.QueryParser

open Querarter.Domain.Transformation

module TransformationParser =
    let private aggregateStr = "Aggregate"
    let private convertStr = "Convert"

    let private notStringRepresentation what kind =
        sprintf "%s '%A' does not have a text representation." kind what

    let aggregateFrom (qualifier: string) =
        match (qualifier.ToLowerInvariant()) with
        | "average" -> Aggregate.Average
        | "count" -> Aggregate.Count
        | "sum" -> Aggregate.Sum
        | _ -> Aggregate.UnknownAggregate

    let aggregateName (aggregate: Aggregate) =
        match aggregate with
        | Aggregate.Implementation -> raise (new System.NotImplementedException())
        | Aggregate.Average -> "average"
        | Aggregate.Count -> "count"
        | Aggregate.Sum -> "sum"
        | other -> failwith (notStringRepresentation other aggregateStr)

    let convertFrom (qualifier: string) =
        match (qualifier.ToLowerInvariant()) with
        | "identity" -> Convert.Identity
        | "str_length" -> Convert.StrLength
        | "time_posix" -> Convert.TimePosix
        | _ -> Convert.UnknownConvert

    let convertName (convert: Convert) =
        match convert with
        | Convert.Implementation -> raise (new System.NotImplementedException())
        | Convert.Identity -> "identity"
        | Convert.StrLength -> "str_length"
        | Convert.TimePosix -> "time_posix"
        | other -> failwith (notStringRepresentation other convertStr)
