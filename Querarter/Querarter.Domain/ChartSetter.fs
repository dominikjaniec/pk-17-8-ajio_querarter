﻿namespace Querarter.Domain

open Querarter.Domain.ChartData

module ChartSetter =
    let withSeriesAs chart newSeries =
        { chart with Series = newSeries }

    let withSeriesNameAs chart newName =
        withSeriesAs chart { chart.Series with Name = newName }

    let withSeriesFromAs chart newFrom =
        withSeriesAs chart { chart.Series with From = newFrom }

    let withSeriesFromColumnsAs chart newColumns =
        withSeriesFromAs chart { chart.Series.From with Columns = newColumns }

    let withSeriesFromColumnsSourceAs chart newSource =
        withSeriesFromColumnsAs chart { chart.Series.From.Columns with Source = newSource }

    let withSeriesFromColumnAliasAs chart newAlias =
        withSeriesFromColumnsAs chart { chart.Series.From.Columns with Alias = newAlias }

    let withSeriesFromConvertAs chart newConvert =
        withSeriesFromAs chart { chart.Series.From with Convert = newConvert }

    let withSeriesAggregateAs chart newAggregate =
        withSeriesAs chart { chart.Series with Aggregate = Some newAggregate }

    let withOverAs chart newOver =
        { chart with Over = Some newOver }

    let withOverNumbersFromAs chart newFrom =
        withOverAs chart (Numbers (newFrom, 1M))

    let withOverNumbersFromWithStepAs chart newStep =
        let currentFromValue =
            match chart.Over with
            | Some (Numbers (from, _)) -> from
            | _ -> 0M
        withOverAs chart (Numbers (currentFromValue, newStep))

    let withTitlesAs chart newTitles =
        { chart with Titles = Some newTitles }

    let withTitlesTopAs chart newTop =
        match chart.Titles with
        | Some titles -> withTitlesAs chart { titles with Top = Some newTop }
        | None -> withTitlesAs chart { Top = Some newTop; Bottom = None; LeftSide = None; RightSide = None }

    let withTitlesBottomAs chart newBottom =
        match chart.Titles with
        | Some titles -> withTitlesAs chart { titles with Bottom = Some newBottom }
        | None -> withTitlesAs chart { Top = None; Bottom = Some newBottom; LeftSide = None; RightSide = None }

    let withTitlesLeftSideAs chart newLeftSide =
        match chart.Titles with
        | Some titles -> withTitlesAs chart { titles with LeftSide = Some newLeftSide }
        | None -> withTitlesAs chart { Top = None; Bottom = None; LeftSide = Some newLeftSide; RightSide = None }

    let withTitlesRightSideAs chart newRightSide =
        match chart.Titles with
        | Some titles -> withTitlesAs chart { titles with RightSide = Some newRightSide }
        | None -> withTitlesAs chart { Top = None; Bottom = None; LeftSide = None; RightSide = Some newRightSide }
