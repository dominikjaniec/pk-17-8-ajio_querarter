﻿namespace Querarter.Domain

module Transformation =
    type Aggregate =
        | UnknownAggregate = 0
        | Implementation = 1
        | Average = 10
        | Count = 11
        | Sum = 12

    type Convert =
        | UnknownConvert = 0
        | Implementation = 1
        | Identity = 10
        | StrLength = 12
        | TimePosix = 13
