﻿namespace Querarter.Domain

open Transformation

module ChartData =
    type ColumnSource =
        | ColumnIndex of int
        | ColumnHeader of string

    type Column = {
        Source: ColumnSource;
        Alias: string;
    }

    type From = {
        Columns: Column;
        Convert: Convert;
    }

    type Series = {
        Name: string;
        From: From;
        Aggregate: Aggregate option;
    }

    type Over =
        | Numbers of decimal * decimal

    type Titles = {
        Top: string option;
        Bottom: string option;
        LeftSide: string option;
        RightSide: string option;
    }

    type Chart = {
        Series: Series;
        Over: Over option;
        Titles: Titles option;
    }
