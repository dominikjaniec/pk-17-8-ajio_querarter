﻿namespace Querarter.Domain

module Syntax =
    type Keyword =
        | UnknownKeyword = 0
        | CodeBlockBegin = 1
        | CodeBlockEnd = 2
        | SERIES = 101
        | FROM = 102
        | AS = 103
        | CONVERTED = 104
        | TO = 105
        | AGGREGATED = 106
        | WITH = 107
        | OVER = 108
        | NUMBERS = 109
        | STEP = 110
        | VALUES = 111
        | TITLED = 112
        | DESCRIBED = 113
        | LEFT = 114
        | RIGHT = 115

    type GroupId =
        | UnknownGroup
        | SyntaxTree
        | Series
        | SeriesName
        | SeriesFrom
        | SeriesFromAs
        | SeriesFromConvertedTo
        | SeriesAggregateWith
        | Over
        | OverNumbersFrom
        | OverNumbersFromWithStep
        | Titled
        | Described
        | DescribedAs
        | DescribedOver
        | DescribedOverLeft
        | DescribedOverRight

    type GroupDefinition<'a> = {
        Id: GroupId;
        Req: bool;
        Nodes: 'a list
    }

    type SyntaxNode =
        | Group of GroupDefinition<SyntaxNode>
        | Choice of SyntaxNode list
        | Keyword of Keyword
        | Qualifier
        | UserText

    let syntaxTree =
        Group { Id = SyntaxTree; Req = true; Nodes = [
            Group { Id = Series; Req = true; Nodes = [
                Group { Id = SeriesName; Req = false; Nodes = [
                    (Keyword Keyword.SERIES)
                    UserText
                ]}
                Group { Id = SeriesFrom; Req = true; Nodes = [
                    (Keyword Keyword.FROM)
                    Choice [ Qualifier; UserText ]
                    Group { Id = SeriesFromAs; Req = false; Nodes = [
                        (Keyword Keyword.AS)
                        Qualifier
                    ]}
                    Group { Id = SeriesFromConvertedTo; Req = false; Nodes = [
                        (Keyword Keyword.CONVERTED)
                        (Keyword Keyword.TO)
                        Choice [ Qualifier ]
                    ]}
                ]}
                Group { Id = SeriesAggregateWith; Req = false; Nodes = [
                    (Keyword Keyword.AGGREGATED)
                    (Keyword Keyword.WITH)
                    Choice [ Qualifier ]
                ]}
            ]}
            Group { Id = Over; Req = false; Nodes = [
                (Keyword Keyword.OVER)
                Choice [
                    Group { Id = OverNumbersFrom; Req = true; Nodes = [
                        (Keyword Keyword.NUMBERS)
                        (Keyword Keyword.FROM)
                        Qualifier
                        Group { Id = OverNumbersFromWithStep; Req = false; Nodes = [
                            (Keyword Keyword.WITH)
                            (Keyword Keyword.STEP)
                            Qualifier
                        ]}
                    ]}
                ]
            ]}
            Group { Id = Titled; Req = false; Nodes = [
                (Keyword Keyword.TITLED)
                UserText
            ]}
            Group { Id = Described; Req = false; Nodes = [
                (Keyword Keyword.DESCRIBED)
                Group { Id = DescribedAs; Req = false; Nodes = [
                    (Keyword Keyword.AS)
                    UserText
                ]}
                Group { Id = DescribedOver; Req = false; Nodes = [
                    (Keyword Keyword.OVER)
                    Group { Id = DescribedOverLeft; Req = false; Nodes = [
                        (Keyword Keyword.LEFT)
                        UserText
                    ]}
                    Group { Id = DescribedOverRight; Req = false; Nodes = [
                        (Keyword Keyword.RIGHT)
                        UserText
                    ]}
                ]}
            ]}
        ]}
