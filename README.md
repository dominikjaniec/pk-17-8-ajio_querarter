# AJiO Laboratorium - Automaty, Języki i Obliczenia
## PK FMI Semestr VIII (studia niestacjonarne), 2016/2017

----

# **Querarter**: Query Based Chart Generator
## Generator wykresów na podstawie zbioru danych z wykorzystaniem prostego języka zapytań

Prosty system generowania wykresów na bazie postępów graczy lub innych danych "statystycznych", pozwoli urozmaicić podsumowania turniejów, jak i strony domowe danych graczy.

Zrealizowany program będzie przetwarzać podany zbiór danych na reprezentację graficzną, na podstawie wyrażenia-zapytania opisującego wykres.

Program zostanie napisany z wykorzystaniem języka
o paradygmacie funkcyjnym: F# na platformie .NET Core.
Będzie to program konsolowy, uruchamiany wraz z wyrażeniem opisującym wykres, oraz danymi źródłowymi w formacie CSV.

----
## Help page source: ( [HTML](https://bitbucket.org/dominikjaniec/pk-ajio_qbcg/src/e35620e7fc93f6bb885c8f71dab0ef93d2b7a05a/QuerySyntax.html?fileviewer=file-view-default) | [PNG](https://bitbucket.org/dominikjaniec/pk-ajio_qbcg/src/e35620e7fc93f6bb885c8f71dab0ef93d2b7a05a/QuerySyntax.png?fileviewer=file-view-default) )

![QuerySyntax.png](https://bitbucket.org/repo/gkbyagG/images/3604901002-QuerySyntax.png)

----
## Command line interface reference:

```
Querarter: Query Based Chart Generator v. alpha-0.0.0.1
Copyright (C) 2017 Dominik Janiec

  make       Make chart based on query and data source.
  parse      Parse given query and show chart's description.
  convert    Convert data source with the aid of query.


`$ querarter make`     Make chart based on query and data source.

  -e, --output-format  (Default: png) Generated chart's file format.
  -i, --input          Input source data's file path, otherwise STDIN will be
                       used instead as source.
  -s, --input-format   (Default: csv) Source data's file format.
  -o, --output         Required. Generated result's file path.
  -f, --override       (Default: False) Force override generated output.
  -q, --query          Required. Given query's file path.
  -g                   (Default: False) Query will be interpreted as given,


`$ querarter parse`    Parse given query and show chart's description.

  -p, --permissive     (Default: False) Skip chart's logic validation.
  -q, --query          Required. Given query's file path.
  -g                   (Default: False) Query will be interpreted as given, not
                       as file path.

`$ querarter convert`  Convert data source with the aid of query.

  -e, --output-format  (Default: csv) Converted data's file format.
  -i, --input          Input source data's file path, otherwise STDIN will be
                       used instead as source.
  -s, --input-format   (Default: csv) Source data's file format.
  -o, --output         Required. Generated result's file path.
  -f, --override       (Default: False) Force override generated output.
  -q, --query          Required. Given query's file path.
  -g                   (Default: False) Query will be interpreted as given,
                       not as file path.
```
